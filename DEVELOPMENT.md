# ORM - nah..

# TODO:
 - write tests
 - body validation - no time to make it perfect.....
 - Error handler to catch exceptions
 - log out !important
 - when billy is down do not stop the request?
 - UI error handling
 - split synchronization for contacts/products when you add products..

# DONE:
 - phpunit
 - Angular with CLI - ng new..
 - removed organizationId............
 - base http service !important
 - make uniqueId in contact required
 - config file
 - DI container
 - update README
 - json parser [here](https://www.slimframework.com/docs/v4/objects/request.html#the-request-body)