<?php

namespace App\Infrastructure\Exceptions;

use Exception;
use Throwable;

/**
 * Class NotFoundException
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
class NotFoundException extends Exception
{
    /**
     * NotFoundException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "Not found", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
