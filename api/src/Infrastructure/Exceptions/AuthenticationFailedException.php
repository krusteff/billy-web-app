<?php

namespace App\Infrastructure\Exceptions;

use Exception;
use Throwable;

/**
 * Class AuthenticationFailedException
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
class AuthenticationFailedException extends Exception
{
    /**
     * AuthenticationFailedException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "Authentication failed", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
