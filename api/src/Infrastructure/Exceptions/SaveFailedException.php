<?php

namespace App\Infrastructure\Exceptions;

use Exception;
use Throwable;

/**
 * Class SaveFailedException
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
class SaveFailedException extends Exception
{
    /**
     * SaveFailedException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "Save failed", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
