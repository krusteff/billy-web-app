<?php

namespace App\Infrastructure\Exceptions;

use Exception;
use Throwable;

/**
 * Class UnprocessableModelException
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
class UnprocessableModelException extends Exception
{
    /**
     * UnprocessableModelException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "Unprocessable model", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
