<?php

namespace App\Infrastructure\Exceptions;

use Exception;
use Throwable;

/**
 * Class DeleteFailedException
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
class DeleteFailedException extends Exception
{
    /**
     * DeleteFailedException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "Delete failed", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
