<?php

namespace App\Infrastructure\Middlewares;

use App\Services\AuthService;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Class AuthMiddleware
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
class AuthMiddleware implements MiddlewareInterface
{
    /**
     * @var AuthService
     */
    private $authService;

    /**
     * AuthMiddleware constructor.
     *
     * @param AuthService $authService
     */
    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    /**
     * @inheritDoc
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $token = $request->getHeaderLine('X-Access-Token');

        if (!$token) {
            return new JsonResponse(['Authorization' => 'failed']);
        }

        $user = $this->authService->authenticate($token);

        if (!$user) {
            return new JsonResponse(['Authorization' => 'failed']);
        }

        return $handler->handle($request);
    }
}