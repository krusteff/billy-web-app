<?php

namespace App\Infrastructure;

/**
 * Class Enum
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
abstract class Enum
{
    public static $MAPPING = [];

    /**
     * @param int $status
     *
     * @return string|null
     */
    public static function getLabel(int $status): ?string
    {
        return static::$MAPPING[$status] ?? null;
    }

    /**
     * @param string $label
     *
     * @return string|null
     */
    public static function getValue(string $label): ?string
    {
        foreach (static::$MAPPING as $key => $value) {
            if (strtolower($value) === strtolower($label)) {
                return $key;
            }
        }

        return null;
    }
}
