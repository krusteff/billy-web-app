<?php

namespace App\Infrastructure;

use App\Infrastructure\Exceptions\NotFoundException;
use App\Infrastructure\Exceptions\UnprocessableModelException;
use App\Services\AuthService;
use Exception;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

/**
 * Class BillyConnector
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
class BillyConnector
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var AuthService
     */
    private $authService;

    /**
     * BillyConnector constructor.
     *
     * @param Client $client
     * @param AuthService $authService
     */
    public function __construct(Client $client, AuthService $authService)
    {
        $this->client = $client;
        $this->authService = $authService;
    }

    /**
     * @return array
     */
    public function getContacts(): array
    {
        return $this->getAll('contacts');
    }

    /**
     * @return array
     */
    public function getProducts(): array
    {
        return $this->getAll('products');
    }

    /**
     * @return array
     */
    public function getAllCountries(): array
    {
        return $this->getAll('countries');
    }

    /**
     * @param string $uniqueId
     *
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function hasContact(string $uniqueId): bool
    {
        $options = ['headers' => $this->getDefaultHeaders(), 'http_errors' => false];
        $response = $this->client->get('contacts/' . $uniqueId, $options);

        return $this->formatResponse($response, 'contact') !== null;
    }

    /**
     * Creates contact
     *
     * @param array $data
     *
     * @return array
     */
    public function createContact(array $data)
    {
        return $this->create('contacts', $data);
    }

    /**
     * Creates product
     *
     * @param array $data
     *
     * @return array
     */
    public function createProduct(array $data)
    {
        return $this->create('products', $data);
    }

    /**
     * @param string $uniqueId
     * @param array $data
     *
     * @return mixed
     */
    public function updateContact(string $uniqueId, array $data): array
    {
        return $this->update('contacts', $uniqueId, $data);
    }

    /**
     * Updates product
     *
     * @param string $uniqueId
     * @param array $data
     *
     * @return array
     */
    public function updateProduct(string $uniqueId, array $data): array
    {
        return $this->update('products', $uniqueId, $data);
    }

    /**
     * Deletes contact
     *
     * @param string $uniqueId
     *
     * @return bool
     */
    public function deleteContact(string $uniqueId): bool
    {
        return $this->deleteContact('contacts', $uniqueId);
    }

    /**
     * Deletes product
     *
     * @param string $uniqueId
     *
     * @return bool
     */
    public function deleteProduct(string $uniqueId): bool
    {
        return $this->delete('products', $uniqueId);
    }

    /**
     * Returns list
     *
     * @param string $resource
     *
     * @return NotFoundException|array
     */
    private function getAll(string $resource)
    {
        $options = ['headers' => $this->getDefaultHeaders(), 'http_errors' => false];
        $response = $this->client->get($resource, $options);

        return $this->formatResponse($response, $resource);
    }

    /**
     * Creates resource
     *
     * @param string $resource
     * @param $data
     *
     * @return array
     */
    private function create(string $resource, $data): array
    {
        $options = ['headers' => $this->getDefaultHeaders(), 'json' => [substr($resource, 0, -1) => $data], 'http_errors' => false];
        $response = $this->client->post($resource, $options);

        return $this->formatResponse($response, $resource)[0];
    }

    /**
     * Updates resource
     *
     * @param string $resource
     * @param string $uniqueId
     * @param array $data
     *
     * @return array
     */
    private function update(string $resource, string $uniqueId, array $data): array
    {
        $options = ['headers' => $this->getDefaultHeaders(), 'json' => [substr($resource, 0, -1) => $data], 'http_errors' => false];
        $response = $this->client->put($resource . '/' . $uniqueId, $options);

        return $this->formatResponse($response, $resource)[0];
    }

    /**
     * Deletes resource
     *
     * @param string $resource
     * @param string $uniqueId
     *
     * @return bool
     */
    private function delete(string $resource, string $uniqueId): bool
    {
        $response = $this->client->delete($resource . '/' . $uniqueId, [
            'headers' => $this->getDefaultHeaders(),
            'http_errors' => false,
        ]);

        $formattedResponse = $this->formatResponse($response);

        return key_exists('meta', $formattedResponse) && key_exists('deletedRecords', $formattedResponse['meta']);
    }

    /**
     * Formats response into JSON
     *
     * @param ResponseInterface $response
     * @param string|null $key
     *
     * @return array
     */
    private function formatResponse(ResponseInterface $response, string $key = null): array
    {
        if ($response->getStatusCode() === 200) {
            $decodedBody = json_decode($response->getBody()->getContents(), true);

            if ($key !== null) {
                return $decodedBody[$key];
            }

            return $decodedBody;
        }

        if ($response->getStatusCode() === 404) {
            return new NotFoundException();
        }

        if ($response->getStatusCode() === 422) {
            throw new UnprocessableModelException();
        }

        // TODO: handle better

        throw new Exception();
    }

    /**
     * @return array
     */
    private function getDefaultHeaders(): array
    {
        return ['X-Access-Token' => $this->authService->getLoggedUser()->getToken()];
    }
}
