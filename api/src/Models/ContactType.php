<?php

namespace App\Enums;

use App\Infrastructure\Enum;

/**
 * Class ApplicationStatus
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
final class ContactType extends Enum
{
    public const PERSON = 1;
    public const COMPANY = 2;
    public static $MAPPING = [
        self::PERSON => 'Person',
        self::COMPANY => 'Company',
    ];
}
