<?php

namespace App\Models;

/**
 * Class Contact
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
class Contact implements ModelInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $uniqueId;

    /**
     * @var int
     */
    private $type;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $countryId;

    /**
     * @var int
     */
    private $userId;

    public function getData(): array
    {
        return [
            'id' => $this->getId(),
            'uniqueId' => $this->getUniqueId(),
            'type' => $this->getType(),
            'name' => $this->getName(),
            'countryId' => $this->getCountryId(),
            'userId' => $this->getUserId(),
        ];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Contact
     */
    public function setId(int $id): Contact
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getUniqueId(): string
    {
        return $this->uniqueId;
    }

    /**
     * @param string $uniqueId
     *
     * @return Contact
     */
    public function setUniqueId(string $uniqueId): Contact
    {
        $this->uniqueId = $uniqueId;

        return $this;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     *
     * @return Contact
     */
    public function setType(int $type): Contact
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Contact
     */
    public function setName(string $name): Contact
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getCountryId(): string
    {
        return $this->countryId;
    }

    /**
     * @param string $countryId
     *
     * @return Contact
     */
    public function setCountryId(string $countryId): Contact
    {
        $this->countryId = $countryId;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     *
     * @return Contact
     */
    public function setUserId(int $userId): Contact
    {
        $this->userId = $userId;

        return $this;
    }
}
