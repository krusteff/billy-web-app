<?php

namespace App\Models;

/**
 * Class Product
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
class Product implements ModelInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $uniqueId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $userId;

    /**
     * @inheritDoc
     */
    public function getData(): array
    {
        return [
            'id' => $this->getId(),
            'uniqueId' => $this->getUniqueId(),
            'name' => $this->getName(),
            'userId' => $this->getUserId(),
        ];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Contact
     */
    public function setId(int $id): Contact
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getUniqueId(): string
    {
        return $this->uniqueId;
    }

    /**
     * @param string $uniqueId
     *
     * @return Contact
     */
    public function setUniqueId(string $uniqueId): Contact
    {
        $this->uniqueId = $uniqueId;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Contact
     */
    public function setName(string $name): Contact
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     *
     * @return Contact
     */
    public function setUserId(int $userId): Contact
    {
        $this->userId = $userId;

        return $this;
    }
}
