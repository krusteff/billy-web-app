<?php

namespace App\Models;

/**
 * Interface ModelInterface
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
interface ModelInterface
{
    /**
     * Returns unique ID
     * @return string
     */
    public function getUniqueId(): string;

    /**
     * Returns user's ID
     * @return int
     */
    public function getUserId(): int;

    /**
     * Returns model's data
     * @return array
     */
    public function getData(): array;
}
