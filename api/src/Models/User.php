<?php

namespace App\Models;

/**
 * Class User
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
class User implements ModelInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $token;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     *
     * @return User
     */
    public function setToken(string $token): User
    {
        $this->token = $token;

        return $this;
    }

    public function getUniqueId(): string
    {
        return '';
    }

    public function getUserId(): int
    {
        return $this->id;
    }

    public function getData(): array
    {
        return [];
    }
}
