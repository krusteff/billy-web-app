<?php

namespace App\Services;

use App\Models\User;

/**
 * Class UserService
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
class UserService extends AbstractDBService
{
    protected $tableName = 'users';

    protected $modelClass = User::class;

    /**
     * Find user by token
     *
     * @param string $token
     *
     * @return User|null
     */
    public function getByToken(string $token): ?User
    {
        return $this->getOneBy('token', $token);
    }
}
