<?php

namespace App\Services;

use App\Models\Contact;

/**
 * Class ContactService
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
class ContactService extends AbstractDBService
{
    protected $tableName = 'contacts';

    protected $modelClass = Contact::class;
}
