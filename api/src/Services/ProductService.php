<?php

namespace App\Services;

use App\Models\Product;

/**
 * Class ProductService
 *
 * @author Martin Krastev <m.krastev@gmail.com>
 */
class ProductService extends AbstractDBService
{
    protected $tableName = 'products';

    protected $modelClass = Product::class;
}
