<?php

namespace App\Services;

use App\Infrastructure\Exceptions\SaveFailedException;
use App\Models\ModelInterface;
use Config\Config;
use Exception;
use InvalidArgumentException;
use PDO;
use PDOException;

/**
 * Class DBService
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
abstract class AbstractDBService implements DBServiceInterface
{
    /**
     * @var string
     */
    protected $tableName = '';

    /**
     * @var string
     */
    protected $modelClass = '';

    /**
     * @var PDO
     */
    private $connection;

    /**
     * @return PDO
     */
    public function getConnection(): PDO
    {
        if ($this->connection === null) {
            $dbConfig = Config::$DB;
            $servername = $dbConfig['servername'];
            $username = $dbConfig['username'];
            $password = $dbConfig['password'];
            $dbName = $dbConfig['dbName'];

            try {
                $this->connection = new PDO("mysql:host=$servername;dbname=$dbName", $username, $password);
                $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (PDOException $e) {
                echo "Connection failed: " . $e->getMessage();
            }
        }

        return $this->connection;
    }

    /**
     * @inheritDoc
     */
    public function getAll(array $by = []): array
    {
        $db = $this->getConnection();
        $where = '';

        if (count($by)) {
            $clauses = [];

            foreach ($by as $column => $value) {
                $clauses[] = "$column = :$column";
            }

            $where = implode(" AND ", $clauses);
        }

        $sql = "SELECT * FROM $this->tableName" . ($where !== '' ? ' WHERE ' . $where : '');
        $stmt = $db->prepare($sql);
        $stmt->execute($by);

        return $stmt->fetchAll(PDO::FETCH_CLASS, $this->modelClass);
    }

    /**
     * @inheritDoc
     */
    public function getById(int $id): ?ModelInterface
    {
        return $this->getAll(['id' => $id])[0] ?? null;
    }

    /**
     * @inheritDoc
     */
    public function getOneBy($column, $value): ?ModelInterface
    {
        return $this->getAll([$column => $value])[0] ?? null;
    }

    /**
     * @inheritDoc
     */
    public function create(array $data): ModelInterface
    {
        $db = $this->getConnection();
        $columns = array_keys($data);
        $columnsNames = implode(',', $columns);
        $columnsBinds = ':' . implode(',:', $columns);

        try {
            $sql = "INSERT INTO $this->tableName ($columnsNames) VALUES ($columnsBinds)";

            $prepared = $db->prepare($sql);
            $prepared->execute($data);

            return $this->getById($db->lastInsertId());
        } catch (Exception $e) {
            throw new SaveFailedException();
        }
    }

    public function update(int $id, array $data): ModelInterface
    {
        $db = $this->getConnection();

        if (!count($data)) {
            throw new InvalidArgumentException('Data can not be empty');
        }

        $clauses = [];

        foreach ($data as $column => $value) {
            $clauses[] = "$column = :$column";
        }

        $updates = implode(", ", $clauses);

        try {
            $sql = "UPDATE $this->tableName SET $updates WHERE id = :id";
            $prepared = $db->prepare($sql);
            $prepared->execute(array_merge($data, ['id' => $id]));

            return $this->getById($id);
        } catch (Exception $e) {
            throw new SaveFailedException();
        }
    }

    public function delete(int $id): bool
    {
        $db = $this->getConnection();

        try {
            $sql = "DELETE FROM $this->tableName WHERE id = :id";

            $prepared = $db->prepare($sql);
            $prepared->execute(['id' => $id]);

            return true;
        } catch (Exception $e) {
            return false;
        }
    }
}
