<?php

namespace App\Services;

use App\Models\User;

/**
 * Class AuthService
 *
 * @author Martin Krastev <m.krastev@gmail.com>
 */
class AuthService
{
    /**
     * @var User
     */
    private $loggedUser;

    /**
     * @var UserService
     */
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @param string $token
     *
     * @return User
     * @throws \App\Infrastructure\Exceptions\SaveFailedException
     */
    public function authenticate(string $token): User
    {
        $user = $this->userService->getByToken($token);

        if (!$user) {
            $user = $this->userService->create(['token' => $token]);
        }

        return $this->loggedUser = $user;
    }

    /**
     * @return User
     */
    public function getLoggedUser(): User
    {
        return $this->loggedUser;
    }
}
