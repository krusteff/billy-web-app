<?php

namespace App\Services;

use App\Models\ModelInterface;

/**
 * Interface DBServiceInterface
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
interface DBServiceInterface
{
    /**
     * Returns all models
     *
     * @param array $by criteria in format ['columnName' => $value]
     *
     * @return ModelInterface[]
     */
    public function getAll(array $by = []): array;

    /**
     * @param int $id
     *
     * @return ModelInterface|null
     */
    public function getById(int $id): ?ModelInterface;

    public function getOneBy($column, $value): ?ModelInterface;

    public function create(array $data): ModelInterface;

    public function update(int $id, array $data): ModelInterface;

    public function delete(int $id): bool;
}
