<?php

namespace App\Handlers;

use App\Services\AuthService;
use App\Services\DBServiceInterface;
use Psr\Container\ContainerInterface;

/**
 * Class AbstractHandler
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
abstract class AbstractHandler implements HandlerInterface
{
    /**
     * @var DBServiceInterface
     */
    protected $service;

    /**
     * @var AuthService
     */
    protected $authService;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * AbstractViewHandler constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->authService = $container->get(AuthService::class);
    }
}
