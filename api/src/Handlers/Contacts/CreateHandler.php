<?php

namespace App\Handlers\Contacts;

use App\Enums\ContactType;
use App\Handlers\AbstractCreateHandler;
use App\Services\ContactService;
use Exception;
use Psr\Container\ContainerInterface;

/**
 * Class CreateHandler
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
class CreateHandler extends AbstractCreateHandler
{
    /**
     * CreateHandler constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        $this->service = $container->get(ContactService::class);
    }

    /**
     * @param array $data
     *
     * @throws Exception
     */
    protected function validateBody(array $data)
    {
        $validationFailed = [];

        if (!key_exists('name', $data) || empty($data['name'])) {
            $validationFailed['name'] = 'Field `name` is required';
        }
        if (!key_exists('type', $data)
            || empty($data['type'])
            || !in_array($data['type'], [ContactType::PERSON, ContactType::COMPANY])) {
            $validationFailed['type'] = 'Field `type` is required or failed haystack';
        }
        if (!key_exists('countryId', $data) || empty($data['countryId'])) {
            $validationFailed['countryId'] = 'Field `countryId` is required';
        }

        if (!empty($validationFailed)) {
            throw new Exception(json_encode($validationFailed), 409);
        }
    }

    /**
     * @inheritDoc
     */
    protected function createBillyResource(array $data): array
    {
        $data['type'] = strtolower(ContactType::getLabel($data['type']));

        return $this->billyConnector->createContact($data);
    }

    /**
     * @inheritDoc
     */
    protected function deleteBillyResource(string $id): void
    {
        $this->billyConnector->deleteContact($id);
    }
}
