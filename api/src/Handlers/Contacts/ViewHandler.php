<?php

namespace App\Handlers\Contacts;

use App\Handlers\AbstractViewHandler;
use App\Services\ContactService;
use Psr\Container\ContainerInterface;

/**
 * Class ViewHandler
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
class ViewHandler extends AbstractViewHandler
{
    /**
     * IndexHandler constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        $this->service = $container->get(ContactService::class);
    }
}
