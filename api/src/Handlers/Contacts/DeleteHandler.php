<?php

namespace App\Handlers\Contacts;

use App\Handlers\AbstractDeleteHandler;
use App\Services\ContactService;
use Psr\Container\ContainerInterface;

/**
 * Class DeleteHandler
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
class DeleteHandler extends AbstractDeleteHandler
{
    /**
     * IndexHandler constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        $this->service = $container->get(ContactService::class);
    }

    /**
     * @inheritDoc
     */
    protected function deleteBillyResource(string $id): void
    {
        $this->billyConnector->deleteContact($id);
    }
}
