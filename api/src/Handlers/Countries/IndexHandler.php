<?php

namespace App\Handlers\Countries;

use App\Handlers\HandlerInterface;
use App\Infrastructure\BillyConnector;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class IndexHandler
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
class IndexHandler implements HandlerInterface
{
    /**
     * @var BillyConnector
     */
    private $billyConnector;

    /**
     * IndexHandler constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->billyConnector = $container->get(BillyConnector::class);
    }

    /**
     * @inheritDoc
     */
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        $countries = $this->billyConnector->getAllCountries();

        return new JsonResponse($countries);
    }
}
