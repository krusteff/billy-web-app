<?php

namespace App\Handlers;

use App\Infrastructure\Exceptions\NotFoundException;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class AbstractViewHandler
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
abstract class AbstractViewHandler extends AbstractHandler
{
    /**
     * @inheritDoc
     */
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        $id = $request->getAttribute('id');
        $user = $this->authService->getLoggedUser();
        $model = $this->service->getById($id);

        if (!$model || $model->getUserId() !== $user->getId()) {
            throw new NotFoundException();
        }

        return new JsonResponse($model->getData());
    }
}
