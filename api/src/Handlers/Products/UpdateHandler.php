<?php

namespace App\Handlers\Products;

use App\Handlers\AbstractUpdateHandler;
use App\Models\ModelInterface;
use App\Services\ProductService;
use Exception;
use Psr\Container\ContainerInterface;

/**
 * Class UpdateHandler
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
class UpdateHandler extends AbstractUpdateHandler
{
    /**
     * UpdateHandler constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        $this->service = $container->get(ProductService::class);
    }

    /**
     * @param array $data
     *
     * @throws Exception
     */
    protected function validateBody(array $data): void
    {
        if (!key_exists('name', $data) || empty($data['name'])) {
            throw new Exception();
        }
    }

    /**
     * @inheritDoc
     */
    protected function updateBillyResource(ModelInterface $model, array $data): array
    {
        $id = $model->getUniqueId();

        return $this->billyConnector->updateProduct($id, $data);
    }
}
