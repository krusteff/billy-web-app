<?php

namespace App\Handlers\Products;

use App\Handlers\AbstractIndexHandler;
use App\Services\ProductService;
use Psr\Container\ContainerInterface;

/**
 * Class IndexHandler
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
class IndexHandler extends AbstractIndexHandler
{
    /**
     * IndexHandler constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        $this->service = $container->get(ProductService::class);
    }
}
