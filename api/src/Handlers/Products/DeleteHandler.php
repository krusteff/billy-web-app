<?php

namespace App\Handlers\Products;

use App\Handlers\AbstractDeleteHandler;
use App\Services\ProductService;
use Psr\Container\ContainerInterface;

/**
 * Class DeleteHandler
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
class DeleteHandler extends AbstractDeleteHandler
{
    /**
     * IndexHandler constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        $this->service = $container->get(ProductService::class);
    }

    /**
     * @inheritDoc
     */
    protected function deleteBillyResource(string $id): void
    {
        $this->billyConnector->deleteProduct($id);
    }
}
