<?php

namespace App\Handlers\Products;

use App\Handlers\AbstractCreateHandler;
use App\Services\ProductService;
use Exception;
use Psr\Container\ContainerInterface;

/**
 * Class CreateHandler
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
class CreateHandler extends AbstractCreateHandler
{
    /**
     * CreateHandler constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        $this->service = $container->get(ProductService::class);
    }

    /**
     * @param array $data
     *
     * @throws Exception
     */
    protected function validateBody(array $data): void
    {
        if (!key_exists('name', $data) || empty($data['name'])) {
            throw new Exception();
        }
    }

    /**
     * @inheritDoc
     */
    protected function createBillyResource(array $data): array
    {
        return $this->billyConnector->createProduct($data);
    }

    /**
     * @inheritDoc
     */
    protected function deleteBillyResource(string $id): void
    {
        $this->billyConnector->deleteProduct($id);
    }
}
