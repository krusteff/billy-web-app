<?php

namespace App\Handlers\Products;

use App\Handlers\AbstractViewHandler;
use App\Services\ProductService;
use Psr\Container\ContainerInterface;

/**
 * Class ViewHandler
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
class ViewHandler extends AbstractViewHandler
{
    /**
     * IndexHandler constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        $this->service = $container->get(ProductService::class);
    }
}
