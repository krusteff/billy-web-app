<?php

namespace App\Handlers;

use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class AbstractIndexHandler
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
abstract class AbstractIndexHandler extends AbstractHandler
{
    /**
     * @inheritDoc
     */
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        $contacts = $this->service->getAll($this->criteria());
        $result = array_map(function ($contact) {
            return $contact->getData();
        }, $contacts);

        return new JsonResponse($result);
    }

    /**
     * @return array
     */
    protected function criteria(): array
    {
        return ['userId' => $this->authService->getLoggedUser()->getId()];
    }
}
