<?php

namespace App\Handlers\Synchronize;

use App\Enums\ContactType;
use App\Handlers\HandlerInterface;
use App\Infrastructure\BillyConnector;
use App\Services\AuthService;
use App\Services\ContactService;
use Laminas\Diactoros\Response\EmptyResponse;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class BillyHandler
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
class BillyHandler implements HandlerInterface
{
    /**
     * @var AuthService
     */
    private $authService;

    /**
     * @var ContactService
     */
    private $contactService;

    /**
     * @var BillyConnector
     */
    private $billyConnector;

    /**
     * BillyHandler constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->authService = $container->get(AuthService::class);
        $this->contactService = $container->get(ContactService::class);
        $this->billyConnector = $container->get(BillyConnector::class);
    }

    /**
     * @inheritDoc
     */
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        $loggedUserId = $this->authService->getLoggedUser()->getId();
        $contacts = $this->contactService->getAll(['userId' => $loggedUserId]);

        foreach ($contacts as $contact) {
            $uniqueId = $contact->getUniqueId();
            $data = [
                'type' => strtolower(ContactType::getLabel($contact->getType())),
                'countryId' => $contact->getCountryId(),
                'name' => $contact->getName(),
            ];

            if ($this->billyConnector->hasContact($uniqueId)) {
                $this->billyConnector->updateContact($uniqueId, $data);

                continue;
            }

            $createdContact = $this->billyConnector->createContact($data);

            $data['type'] = $contact->getType();
            $data['uniqueId'] = $createdContact['id'];
            $data['userId'] = $loggedUserId;

            $this->contactService->delete($contact->getId());
            $this->contactService->create($data);
        }

        return new EmptyResponse();
    }
}
