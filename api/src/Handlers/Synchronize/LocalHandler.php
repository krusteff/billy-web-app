<?php

namespace App\Handlers\Synchronize;

use App\Enums\ContactType;
use App\Handlers\HandlerInterface;
use App\Infrastructure\BillyConnector;
use App\Services\AuthService;
use App\Services\ContactService;
use Laminas\Diactoros\Response\EmptyResponse;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class LocalHandler
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
class LocalHandler implements HandlerInterface
{
    /**
     * @var AuthService
     */
    private $authService;

    /**
     * @var ContactService
     */
    private $contactService;

    /**
     * @var BillyConnector
     */
    private $billyConnector;

    /**
     * BillyHandler constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->authService = $container->get(AuthService::class);
        $this->contactService = $container->get(ContactService::class);
        $this->billyConnector = $container->get(BillyConnector::class);
    }

    /**
     * @inheritDoc
     */
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        $billyContacts = $this->billyConnector->getContacts();

        foreach ($billyContacts as $billyContact) {
            $found = $this->contactService->getOneBy('uniqueId', $billyContact['id']);

            $data = [
                'type' => ContactType::getValue($billyContact['type']),
                'name' => $billyContact['name'],
                'countryId' => $billyContact['countryId'],
            ];

            if ($found) {
                $this->contactService->update($found->getId(), $data);

                continue;
            }

            $data['uniqueId'] = $billyContact['id'];
            $data['userId'] = $this->authService->getLoggedUser()->getId();

            $this->contactService->create($data);
        }

        return new EmptyResponse();
    }
}
