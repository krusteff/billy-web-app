<?php

namespace App\Handlers;

use App\Infrastructure\BillyConnector;
use App\Infrastructure\Exceptions\SaveFailedException;
use App\Models\ModelInterface;
use Exception;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class AbstractCreateHandler
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
abstract class AbstractCreateHandler extends AbstractHandler
{
    /**
     * @var BillyConnector
     */
    protected $billyConnector;

    /**
     * AbstractCreateHandler constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        $this->billyConnector = $container->get(BillyConnector::class);
    }

    /**
     * @inheritDoc
     */
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        $requestData = $request->getParsedBody();
        $billyResource = null;

        $this->validateBody($requestData ?? []);

        try {
            $billyResource = $this->createBillyResource($requestData);
            $model = $this->createDBResource($requestData, $billyResource);
        } catch (Exception $e) {
            // fallback mechanism if DB fails
            if ($billyResource !== null) {
                $this->deleteBillyResource($billyResource['id']);
            }

            throw new SaveFailedException();
        }

        return new JsonResponse($model->getData(), 201);
    }

    /**
     * Validates body
     * @param array $data
     */
    protected function validateBody(array $data)
    {
    }

    /**
     * Creates DB resource
     *
     * @param array $requestData
     * @param array $billyData
     *
     * @return ModelInterface
     */
    protected function createDBResource(array $requestData, array $billyData): ModelInterface
    {
        $requestData['uniqueId'] = $billyData['id'];
        $requestData['userId'] = $this->authService->getLoggedUser()->getId();

        return $this->service->create($requestData);
    }

    /**
     * Creates resource in Billy
     *
     * @param array $data
     *
     * @return array
     */
    protected abstract function createBillyResource(array $data): array;

    /**
     * Deletes resource in Billy
     *
     * @param string $id
     *
     * @return array
     */
    protected abstract function deleteBillyResource(string $id): void;
}
