<?php

namespace App\Handlers;

use App\Infrastructure\BillyConnector;
use App\Infrastructure\Exceptions\SaveFailedException;
use App\Models\ModelInterface;
use Exception;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class AbstractUpdateHandler
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
abstract class AbstractUpdateHandler extends AbstractHandler
{
    /**
     * @var BillyConnector
     */
    protected $billyConnector;

    /**
     * AbstractUpdateHandler constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        $this->billyConnector = $container->get(BillyConnector::class);
    }

    /**
     * @inheritDoc
     */
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        $id = $request->getAttribute('id');
        $requestData = $request->getParsedBody();

        $this->validateBody($requestData ?? []);

        try {
            $model = $this->updateDBResource($id, $requestData);

            $this->updateBillyResource($model, $requestData);
        } catch (Exception $e) {
            throw new SaveFailedException();
        }

        return new JsonResponse($model->getData(), 200);
    }

    /**
     * Validates body
     * @param array $data
     */
    protected function validateBody(array $data)
    {
    }

    /**
     * Updates DB resource
     *
     * @param int $id
     * @param array $requestData
     *
     * @return ModelInterface
     */
    protected function updateDBResource(int $id, array $requestData): ModelInterface
    {
        return $this->service->update($id, $requestData);
    }

    /**
     * Updates resource in Billy
     *
     * @param ModelInterface $model
     * @param array $data
     *
     * @return array
     */
    protected abstract function updateBillyResource(ModelInterface $model, array $data): array;
}
