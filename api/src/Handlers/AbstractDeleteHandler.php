<?php

namespace App\Handlers;

use App\Infrastructure\BillyConnector;
use App\Infrastructure\Exceptions\DeleteFailedException;
use App\Infrastructure\Exceptions\NotFoundException;
use Exception;
use Laminas\Diactoros\Response\EmptyResponse;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class AbstractDeleteHandler
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
abstract class AbstractDeleteHandler extends AbstractHandler
{
    /**
     * @var BillyConnector
     */
    protected $billyConnector;

    /**
     * AbstractDeleteHandler constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        $this->billyConnector = $container->get(BillyConnector::class);
    }

    /**
     * @inheritDoc
     */
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        $id = $request->getAttribute('id');

        try {
            $model = $this->service->getById($id);

            if (!$model || $model->getUserId() !== $this->authService->getLoggedUser()->getId()) {
                throw new NotFoundException();
            }

            $this->deleteBillyResource($model->getUniqueId());
            $this->service->delete($id);
        } catch (Exception $e) {
            throw new DeleteFailedException();
        }

        return new EmptyResponse();
    }

    /**
     * Deletes resource in Billy
     *
     * @param string $id
     */
    protected abstract function deleteBillyResource(string $id): void;
}
