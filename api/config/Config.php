<?php

namespace Config;

/**
 * Class Config
 *
 * @author Martin Krastev <m.krastev96@gmail.com>
 */
class Config
{
    public static $DB = [
        'servername' => 'localhost',
        'username' => 'root',
        'password' => '',
        'dbName' => 'billy-app'
    ];
}
