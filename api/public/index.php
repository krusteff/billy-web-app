<?php

use App\Handlers\Contacts as ContactHandlers;
use App\Handlers\Products as ProductHandlers;
use App\Handlers\Countries\IndexHandler;
use App\Handlers\Synchronize as SynchronizeHandlers;
use App\Infrastructure\BillyConnector;
use App\Infrastructure\Logger;
use App\Infrastructure\Middlewares\AuthMiddleware;
use App\Infrastructure\Middlewares\JsonBodyParserMiddleware;
use App\Infrastructure\Middlewares\LazyCORSMiddleware;
use App\Services\AuthService;
use DI\Container;
use GuzzleHttp\Client;
use Psr\Container\ContainerInterface;
use Slim\Factory\AppFactory;

require __DIR__ . '/../vendor/autoload.php';

$container = new Container();
$container->set(BillyConnector::class, function (ContainerInterface $container) {
    $client = new Client(['base_uri' => 'https://api.billysbilling.com/v2/']);
    $authService = $container->get(AuthService::class);

    return new BillyConnector($client, $authService);
});
$app = AppFactory::createFromContainer($container);
$app->add(AuthMiddleware::class);
$app->add(JsonBodyParserMiddleware::class);
$app->addErrorMiddleware(true, true, true, new Logger());

// contacts
$app->get('/contacts', ContactHandlers\IndexHandler::class);
$app->get('/contacts/{id: [0-9]+}', ContactHandlers\ViewHandler::class);
$app->post('/contacts', ContactHandlers\CreateHandler::class);
$app->put('/contacts/{id: [0-9]+}', ContactHandlers\UpdateHandler::class);
$app->delete('/contacts/{id: [0-9]+}', ContactHandlers\DeleteHandler::class);

// products
$app->get('/products', ProductHandlers\IndexHandler::class);
$app->get('/products/{id: [0-9]+}', ProductHandlers\ViewHandler::class);
$app->post('/products', ProductHandlers\CreateHandler::class);
$app->put('/products/{id: [0-9]+}', ProductHandlers\UpdateHandler::class);
$app->delete('/products/{id: [0-9]+}', ProductHandlers\DeleteHandler::class);

// sync
$app->post('/synchronize/local', SynchronizeHandlers\LocalHandler::class);
$app->post('/synchronize/billy', SynchronizeHandlers\BillyHandler::class);
$app->get('/countries', IndexHandler::class);

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(LazyCORSMiddleware::class);

$app->run();