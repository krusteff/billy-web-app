import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  /**
   * Checks f user is logged in
   *
   * @return {boolean}
   */
  public isLogged(): boolean {
    return !!localStorage.getItem('token');
  }

  /**
   *
   * @return {string}
   */
  public get token(): string {
    return localStorage.getItem('token') ?? '';
  }

  /**
   * Logs the user in
   *
   * @param {string} token
   */
  public set login(token: string) {
    localStorage.setItem('token', token);
  }
}
