import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

export abstract class BaseRestService<T> {
  protected endpoint = 'contacts';

  /**
   * Get URL built
   * @return {string}
   * @constructor
   * @protected
   */
  protected get URL(): string {
    return environment.apiURL + this.endpoint;
  }

  /**
   * Get default headers
   * @return {HttpHeaders}
   * @protected
   */
  protected get defaultHeaders(): HttpHeaders {
    return new HttpHeaders({['X-Access-Token']: this.authService.token});
  }

  /**
   * BaseRest service constructor.
   * @param {HttpClient} client
   * @param {AuthService} authService
   * @protected
   */
  protected constructor(protected client: HttpClient,
                        protected authService: AuthService) {
  }

  /**
   * Get all
   * @return {Observable<T[]>}
   */
  public get(): Observable<T[]> {
    return this.client.get<T[]>(this.URL, {headers: this.defaultHeaders});
  }

  /**
   * Get by ID
   * @param {number} id
   * @return {Observable<T>}
   */
  public getById(id: number): Observable<T> {
    return this.client.get<T>(this.URL + '/' + id, {headers: this.defaultHeaders});
  }

  /**
   * Create
   * @param {any[]} data
   * @return {Observable<T>}
   */
  public create(data: any[]): Observable<T> {
    return this.client.post<T>(this.URL, data, {headers: this.defaultHeaders});
  }

  /**
   * Update
   * @param {number} id
   * @param {any[]} data
   * @return {Observable<T>}
   */
  public update(id: number, data: any[]): Observable<T> {
    return this.client.put<T>(this.URL + '/' + id, data, {headers: this.defaultHeaders});
  }

  /**
   * Delete
   * @param {number} id
   * @return {Observable<Object>}
   */
  public delete(id: number) {
    return this.client.delete(this.URL + '/' + id, {headers: this.defaultHeaders});
  }
}