import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ContactInterface } from '../interfaces/contact.interface';
import { BaseRestService } from './base-rest.service';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ContactService extends BaseRestService<ContactInterface> {
  endpoint = 'contacts';

  /**
   * Contact service constructor.
   * @param {HttpClient} client
   * @param {AuthService} authService
   */
  constructor(client: HttpClient,
              authService: AuthService) {
    super(client, authService);
  }
}
