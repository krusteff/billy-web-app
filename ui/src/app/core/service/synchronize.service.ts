import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseRestService } from './base-rest.service';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SynchronizeService extends BaseRestService<any> {
  protected endpoint = 'synchronize';

  /**
   * Synchronize service constructor.
   * @param {HttpClient} client
   * @param {AuthService} authService
   */
  constructor(client: HttpClient,
              authService: AuthService) {
    super(client, authService);
  }

  public fromBilly(): Observable<void> {
    return this.client.post<void>(this.URL + '/local', [], {headers: this.defaultHeaders});
  }

  public toBilly(): Observable<void> {
    return this.client.post<void>(this.URL + '/billy', [], {headers: this.defaultHeaders});
  }
}
