import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseRestService } from './base-rest.service';
import { AuthService } from './auth.service';
import { ProductInterface } from '../interfaces/product.interface';

@Injectable({
  providedIn: 'root'
})
export class ProductService extends BaseRestService<ProductInterface> {
  endpoint = 'products';

  /**
   * Contact service constructor.
   * @param {HttpClient} client
   * @param {AuthService} authService
   */
  constructor(client: HttpClient,
              authService: AuthService) {
    super(client, authService);
  }
}
