import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseRestService } from './base-rest.service';
import { CountryInterface } from '../interfaces/country.interface';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class CountryService extends BaseRestService<CountryInterface> {
  endpoint = 'countries';

  /**
   * Country service constructor.
   * @param {HttpClient} client
   * @param {AuthService} authService
   */
  constructor(client: HttpClient,
              authService: AuthService) {
    super(client, authService);
  }
}
