export interface ContactInterface {
  id: number;
  uniqueId: string;
  name: string;
  countryId: string;
  type: number;
}
