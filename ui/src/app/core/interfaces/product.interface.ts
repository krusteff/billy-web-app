export interface ProductInterface {
  id: number;
  uniqueId: string;
  name: string;
}
