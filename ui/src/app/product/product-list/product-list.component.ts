import { Component, OnInit } from '@angular/core';
import { ProductInterface } from '../../core/interfaces/product.interface';
import { ProductService } from '../../core/service/product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html'
})
export class ProductListComponent implements OnInit {
  public products: ProductInterface[] = [];
  public onGoingSync = false;

  constructor(private service: ProductService) {
  }

  ngOnInit(): void {
    this.getAll();
  }

  /**
   * Delete a contact
   * @param {number} id
   */
  public delete(id: number): void {
    this.service.delete(id).subscribe(() => {
      this.getAll();
    });
  }

  /**
   * Get all contacts
   */
  public getAll(): void {
    this.service.get().subscribe((products: ProductInterface[]) => {
      this.products = products;
    });
  }
}
