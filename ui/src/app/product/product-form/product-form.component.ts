import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ProductService } from '../../core/service/product.service';
import { ProductInterface } from '../../core/interfaces/product.interface';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html'
})
export class ProductFormComponent implements OnInit {
  public form!: FormGroup;
  public productId: number | undefined;

  /**
   * ProductFormComponent constructor.
   * @param {ActivatedRoute} activatedRoute
   * @param {FormBuilder} formBuilder
   * @param {Router} router
   * @param {ProductService} productService
   */
  constructor(private activatedRoute: ActivatedRoute,
              private formBuilder: FormBuilder,
              private router: Router,
              private productService: ProductService) {
    this.createForm();
  }

  /**
   * @inheritDoc
   */
  async ngOnInit(): Promise<void> {
    this.productId = Number(this.activatedRoute.snapshot.paramMap.get('id'));

    if (this.productId) {
      const product = await this.productService.getById(this.productId).toPromise();

      this.createForm(product);
    }
  }

  /**
   * Saves form
   */
  public save(): void {
    if (this.productId) {
      this.productService.update(this.productId, this.form.value).subscribe(() => {
        this.router.navigateByUrl('/products').then();
      });

      return;
    }

    this.productService.create(this.form.value).subscribe(() => {
      this.router.navigateByUrl('/products').then();
    });
  }

  /**
   * Creates form
   *
   * @param {ProductInterface | undefined} product
   */
  private createForm(product?: ProductInterface | undefined): void {
    this.form = this.formBuilder.group({
      name: [product?.name ?? ''],
    });
  }
}
