import { Component, OnInit } from '@angular/core';
import { ContactService } from '../../core/service/contact.service';
import { ContactInterface } from '../../core/interfaces/contact.interface';
import { SynchronizeService } from '../../core/service/synchronize.service';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html'
})
export class ContactListComponent implements OnInit {
  public contacts: ContactInterface[] = [];
  public onGoingSync = false;

  constructor(private service: ContactService,
              private synchronizeService: SynchronizeService) {
  }

  ngOnInit(): void {
    this.getAll();
  }

  /**
   * Delete a contact
   * @param {number} id
   */
  public delete(id: number): void {
    this.service.delete(id).subscribe(() => {
      this.getAll();
    });
  }

  /**
   * Get all contacts
   */
  public getAll(): void {
    this.service.get().subscribe((contacts: ContactInterface[]) => {
      this.contacts = contacts;
    });
  }

  /**
   * Synchronize Billy with the local data
   */
  public synchronizeBilly(): void {
    this.onGoingSync = true;
    this.synchronizeService.toBilly().subscribe(() => {
      this.getAll();
      this.onGoingSync = false;
    });
  }

  /**
   * Synchronize local data with Billy
   */
  public synchronizeLocal(): void {
    this.onGoingSync = true;
    this.synchronizeService.fromBilly().subscribe(() => {
      this.getAll();
      this.onGoingSync = false;
    });
  }
}
