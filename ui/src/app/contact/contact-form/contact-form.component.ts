import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ContactService } from '../../core/service/contact.service';
import { ContactInterface } from '../../core/interfaces/contact.interface';
import { CountryService } from '../../core/service/country.service';
import { CountryInterface } from '../../core/interfaces/country.interface';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html'
})
export class ContactFormComponent implements OnInit {
  public form!: FormGroup;
  public contactId: number | undefined;
  public countries: CountryInterface[] = [];

  /**
   * ContactFormComponent constructor.
   *
   * @param {ActivatedRoute} activatedRoute
   * @param {FormBuilder} formBuilder
   * @param {CountryService} countryService
   * @param {Router} router
   * @param {ContactService} contactService
   */
  constructor(private activatedRoute: ActivatedRoute,
              private formBuilder: FormBuilder,
              private countryService: CountryService,
              private router: Router,
              private contactService: ContactService) {
    this.createForm();
  }

  /**
   * @inheritDoc
   */
  async ngOnInit(): Promise<void> {
    this.contactId = Number(this.activatedRoute.snapshot.paramMap.get('id'));

    this.countryService.get().subscribe((countries: CountryInterface[]) => {
      this.countries = countries;
    });

    if (this.contactId) {
      const contact = await this.contactService.getById(this.contactId).toPromise();

      this.createForm(contact);
    }
  }

  /**
   * Saves form
   */
  public save(): void {
    if (this.contactId) {
      this.contactService.update(this.contactId, this.form.value).subscribe(() => {
        this.router.navigateByUrl('/contacts').then();
      });

      return;
    }

    this.contactService.create(this.form.value).subscribe(() => {
      this.router.navigateByUrl('/contacts').then();
    });
  }

  /**
   * Creates form
   *
   * @param {ContactInterface | undefined} contact
   */
  private createForm(contact?: ContactInterface | undefined): void {
    this.form = this.formBuilder.group({
      name: [contact?.name ?? ''],
      countryId: [contact?.countryId ?? '', [Validators.required]],
      type: [contact?.type ?? null, Validators.required],
    });
  }
}
