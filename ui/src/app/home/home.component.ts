import { Component } from '@angular/core';
import { AuthService } from '../core/service/auth.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent {
  public form: FormGroup;

  public get isLogged(): boolean {
    return this.authService.isLogged();
  }

  constructor(private authService: AuthService) {
    this.form = new FormGroup({
      token: new FormControl('', [Validators.required, Validators.minLength(10)]),
    });
  }

  public login(): void {
    this.authService.login = this.form.value.token;
  }
}
