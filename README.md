# Billy WEB App Task
"Billy WEB App" consists of PHP Rest API built with Slim 4 Framework using PHP 7.2 and UI using Angular 12+

## Prerequisites
- API: 
`PHP 7.2` and `MySQL 5.7 or higher`
- UI:
`Node v14 or higher and npm v6 or higher`

## Setup API
 - Import SQL dump file stored in `sql` folder
 - go to the api folder
 - execute `composer install`
 - configure `config/Config.php` file with DB credentials
 - execute `composer start:dev`

## Setup UI
- go to the ui folder
- execute `npm install`
- execute `npm start`

## Execute tests
TBD